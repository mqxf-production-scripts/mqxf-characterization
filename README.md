Script files for characterizing MQXF mechanical behaviour


** Coil_size program:

*) The size (and gap) template files are available in the Data/Test folder. For new coils they need to be modified using a text editor. Be careful no to modify the file structure and format.


** Cross-sect to ANSYS program:

*) The code needs to be launched with the data of four coils. Both the .txt file and the .size file are required. Examples are found in the Data/Test folder.

*) For the reading of the files, the path can be changed in the code if needed (Line 153, where the program replaces the .txt by .size and looks for the file one level up.). 

** Coilpack size program:

*) For the installation of the pdflatex utility, Miktex can be installed normally in the Windows machine. A syspath.insert line is added below the existing one: sys.path.insert(0, "C:/Users/jferrada/AppData/Local/Programs/MiKTeX/miktex/bin/x64/")

** QP program:

*) To see the channels issue: python qp ./Data/MQXFS6d.ASC -i (Where the filename is an example)

*) To plot searching for every word type the channel name: python qp ./Data/MQXFS6d.ASC -y Coil 204 Azimuthal Strain -o

*) To plot searching for the exact one type "": python qp ./Data/MQXFS6d.ASC -y "Coil 204 Azimuthal Strain" -o

*) To plot the stress add -s and change the channel name to stress

*) For normalization of the x-axis, provide the squared value

*) To plot delta add -d
